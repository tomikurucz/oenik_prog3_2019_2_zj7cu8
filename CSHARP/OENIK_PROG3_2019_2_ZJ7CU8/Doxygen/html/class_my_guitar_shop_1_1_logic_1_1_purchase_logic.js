var class_my_guitar_shop_1_1_logic_1_1_purchase_logic =
[
    [ "PurchaseLogic", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#a4e370609445350a7dcc63dcf56c2e09f", null ],
    [ "PurchaseLogic", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#ab1dc4184588f4169a0ecda7e068db0b1", null ],
    [ "PurchaseLogic", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#ad0106f2d8cf28d83e64b3776b8844a68", null ],
    [ "CreatePurchase", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#a72b137ee6827744e6588f783f20efa48", null ],
    [ "DeletePurchase", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#a5e936c369525cbc34f543458035d3862", null ],
    [ "GetAll", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#aa1a72f21cb1c1f476d2ce1633b567a39", null ],
    [ "GetOne", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#ae49d4a8ee1b97ebf0199c040c323a822", null ],
    [ "PurchasedGuitarData", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#a8ab31182f268c3f6f0d45f9949ec1507", null ],
    [ "Top3HighestValuePurchase", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#ad879f7b49f50a1b20cb70514e5fd16bd", null ],
    [ "UpdateName", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html#a83c2dc6c880caa9a08fb93649f41c26d", null ]
];