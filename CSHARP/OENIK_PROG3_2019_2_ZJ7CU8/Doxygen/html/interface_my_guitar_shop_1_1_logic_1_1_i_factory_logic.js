var interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic =
[
    [ "CreateFactory", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic.html#a5e39c673f654de47ee6d93e94ba6048d", null ],
    [ "DeleteFactory", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic.html#a9b5ad963c6a9bdd759cf2dae40366853", null ],
    [ "GetAll", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic.html#acf443575a1826deb3e63df389db606c6", null ],
    [ "GetOne", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic.html#aa1b222a67c62b76bb7b222afb388f61a", null ],
    [ "UpdateFounderName", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic.html#a25e8643b3903ed357d1ca8c406d77c7e", null ]
];