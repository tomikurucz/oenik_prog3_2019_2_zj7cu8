var class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests =
[
    [ "CreateFactory_Test1", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html#a806de7f49a3b120e503650032682958e", null ],
    [ "CreateFactory_Test2", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html#a80f9a5b23f37805048cb8bed88fbc01e", null ],
    [ "CreateFactory_Test3", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html#a1e4debb3d05051b9dacfac09ae203de5", null ],
    [ "DeleteFactory_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html#a0d28fac9f0bbe40afd61f5c2b8740a22", null ],
    [ "GetAll_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html#a2d7a86cd9e58fd640a8e9e97a5acbdcf", null ],
    [ "GetOne_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html#ac471d50b00daa45e06b40898b925acf4", null ],
    [ "UpdateFounderName_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html#a7c2f67539343b357b65f11b806b6252f", null ]
];