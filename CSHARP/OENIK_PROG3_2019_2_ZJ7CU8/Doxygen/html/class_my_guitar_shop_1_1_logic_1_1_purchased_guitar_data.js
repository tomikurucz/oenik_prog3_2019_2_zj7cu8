var class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data =
[
    [ "ToString", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#a5d488db8a1661f5a36a3609792fd7ab2", null ],
    [ "Brand", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#ae542d4cd86f75ac7641f6733dfad801e", null ],
    [ "Date_of_purchase", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#a5e27a8a586339181e232d210dfc4889d", null ],
    [ "Factory_ID", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#a0f6d545f0f17cccaaf76333c22e026eb", null ],
    [ "Guitar_ID", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#aa861f49f29d8a37d14c055b4daa16663", null ],
    [ "Name", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#a1f7867aa5fd7270422761d7bf844fb33", null ],
    [ "Price", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#acc030e4d7f09ef7ffc7deccdad0cc651", null ],
    [ "Producion_time", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#a8d0d27170675f8e9a7f96eeb00ee8196", null ],
    [ "Style", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#a66e163f66fc483774dc4623d810b6860", null ],
    [ "Type", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html#a7704e99fb2d7f1484a353c2f6e2c32d9", null ]
];