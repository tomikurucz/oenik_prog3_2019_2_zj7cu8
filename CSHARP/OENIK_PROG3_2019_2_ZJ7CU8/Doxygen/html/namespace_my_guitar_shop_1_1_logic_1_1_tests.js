var namespace_my_guitar_shop_1_1_logic_1_1_tests =
[
    [ "CustomerLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests" ],
    [ "FactoryLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests" ],
    [ "GuitarLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests" ],
    [ "PurchaseLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests" ]
];