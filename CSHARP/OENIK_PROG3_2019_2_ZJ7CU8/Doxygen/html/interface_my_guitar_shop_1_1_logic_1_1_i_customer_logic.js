var interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic =
[
    [ "CreateCustomer", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic.html#abd25d43d1fdb83389b896230576af4f2", null ],
    [ "DeleteCustomer", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic.html#acfa8fb64fb373a37872544f9f5e81347", null ],
    [ "GetAll", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic.html#aeb529dd277547fd3753e96b9480fbc5c", null ],
    [ "GetOne", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic.html#ae17da9da9d35b617ca7129fd4edc6263", null ],
    [ "UpdateName", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic.html#a2c48c61906320b73d9287ae096ab268f", null ]
];