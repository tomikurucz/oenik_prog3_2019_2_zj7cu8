var namespace_my_guitar_shop_1_1_data =
[
    [ "Customer", "class_my_guitar_shop_1_1_data_1_1_customer.html", "class_my_guitar_shop_1_1_data_1_1_customer" ],
    [ "DatabaseEntities", "class_my_guitar_shop_1_1_data_1_1_database_entities.html", null ],
    [ "Factory", "class_my_guitar_shop_1_1_data_1_1_factory.html", "class_my_guitar_shop_1_1_data_1_1_factory" ],
    [ "Guitar", "class_my_guitar_shop_1_1_data_1_1_guitar.html", "class_my_guitar_shop_1_1_data_1_1_guitar" ],
    [ "Purchase", "class_my_guitar_shop_1_1_data_1_1_purchase.html", "class_my_guitar_shop_1_1_data_1_1_purchase" ]
];