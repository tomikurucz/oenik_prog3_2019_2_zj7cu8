var interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic =
[
    [ "BrandAveragePrice", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html#a2c4c47c37caf69d85aba8542af6725ac", null ],
    [ "CreateGuitar", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html#aa0bd9d3dd62eae28c1bd58e3e78eb338", null ],
    [ "DeleteGuitar", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html#a82575b067e5a142d1c5e420c31e29b3f", null ],
    [ "GetAll", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html#ab2c872bc9bc79bc9f15eeefd203f572b", null ],
    [ "GetOne", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html#aef758271c67ae2b62375ef0c7d02c8e1", null ],
    [ "UpdatePrice", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html#a9fb473ba5365e075407b07390dcd9a89", null ]
];