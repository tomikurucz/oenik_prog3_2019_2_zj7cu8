var class_my_guitar_shop_1_1_data_1_1_factory =
[
    [ "Factory", "class_my_guitar_shop_1_1_data_1_1_factory.html#a42c0325961cc1eb2c0b905b2387fd90e", null ],
    [ "ToString", "class_my_guitar_shop_1_1_data_1_1_factory.html#a30948e8fab3c133c3a175f6c78ff6042", null ],
    [ "Address", "class_my_guitar_shop_1_1_data_1_1_factory.html#ae32db463a449c909af414a333181f8a2", null ],
    [ "Factory_ID", "class_my_guitar_shop_1_1_data_1_1_factory.html#aeae364317ca1935368e13f569712e6c2", null ],
    [ "Foundation_time", "class_my_guitar_shop_1_1_data_1_1_factory.html#a3e11fe02e3c2fe66e8c72edf497dad9e", null ],
    [ "Founder_name", "class_my_guitar_shop_1_1_data_1_1_factory.html#a94fbe09f8cceae7774845017d49908da", null ],
    [ "Guitar", "class_my_guitar_shop_1_1_data_1_1_factory.html#a48afc1d7996f4f6cfa916bb51574e547", null ],
    [ "Name", "class_my_guitar_shop_1_1_data_1_1_factory.html#a89d722fae46f4d9ed74805bed5201978", null ],
    [ "Phone_number", "class_my_guitar_shop_1_1_data_1_1_factory.html#adfe0d6f2ff3a85b2344f1561ca7aeddf", null ]
];