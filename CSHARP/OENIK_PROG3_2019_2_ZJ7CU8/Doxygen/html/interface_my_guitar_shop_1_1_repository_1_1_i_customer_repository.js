var interface_my_guitar_shop_1_1_repository_1_1_i_customer_repository =
[
    [ "CreateCustomer", "interface_my_guitar_shop_1_1_repository_1_1_i_customer_repository.html#a7ae953e39068176308dc42ff7571193e", null ],
    [ "DeleteCustomer", "interface_my_guitar_shop_1_1_repository_1_1_i_customer_repository.html#a547760eafe5cf26f35e418a3f728c500", null ],
    [ "GetAll", "interface_my_guitar_shop_1_1_repository_1_1_i_customer_repository.html#a7190472fa218f0fc5b1d08c6ea47e313", null ],
    [ "GetOne", "interface_my_guitar_shop_1_1_repository_1_1_i_customer_repository.html#adccea5062086aac7dea9520dcf934194", null ],
    [ "UpdateName", "interface_my_guitar_shop_1_1_repository_1_1_i_customer_repository.html#a3f6233c274b71c443669bb07d60e5975", null ]
];