var class_my_guitar_shop_1_1_data_1_1_purchase =
[
    [ "ToString", "class_my_guitar_shop_1_1_data_1_1_purchase.html#a2442a286bc891a25e00ae23c088cfa38", null ],
    [ "Customer", "class_my_guitar_shop_1_1_data_1_1_purchase.html#a85bf6976b2b10970ab5b1959e4619628", null ],
    [ "Date_of_birth", "class_my_guitar_shop_1_1_data_1_1_purchase.html#a0a6650b326b15d4fa1b8753f17768580", null ],
    [ "Date_of_purchase", "class_my_guitar_shop_1_1_data_1_1_purchase.html#ac781a85c7c7c3ea018ea6d9e886e075c", null ],
    [ "Email", "class_my_guitar_shop_1_1_data_1_1_purchase.html#ae427be9b022f9af702c1779c7484ee43", null ],
    [ "Guitar", "class_my_guitar_shop_1_1_data_1_1_purchase.html#af5558bd585715e7b8899ac82b437377a", null ],
    [ "Guitar_ID", "class_my_guitar_shop_1_1_data_1_1_purchase.html#ac6de60060ef78c5f29a9cf59efa90287", null ],
    [ "Name", "class_my_guitar_shop_1_1_data_1_1_purchase.html#a27a7486fc63daaf692e655351c4128f9", null ],
    [ "Purchase_ID", "class_my_guitar_shop_1_1_data_1_1_purchase.html#a844d3e2d39d73dd1d8cf26d9ef6a07e5", null ]
];