var class_my_guitar_shop_1_1_repository_1_1_customer_repository =
[
    [ "CustomerRepository", "class_my_guitar_shop_1_1_repository_1_1_customer_repository.html#ab8134effbce490e232afb62c56d746ea", null ],
    [ "CreateCustomer", "class_my_guitar_shop_1_1_repository_1_1_customer_repository.html#ad32b422cfe7d5a7293f0c65af68b80b1", null ],
    [ "DeleteCustomer", "class_my_guitar_shop_1_1_repository_1_1_customer_repository.html#a74fe4fdb0549571e5fdeec2bca7ae962", null ],
    [ "GetAll", "class_my_guitar_shop_1_1_repository_1_1_customer_repository.html#ae892220822733cedf9519320ca907786", null ],
    [ "GetOne", "class_my_guitar_shop_1_1_repository_1_1_customer_repository.html#a35fce76c44328fc801094be08a8d692e", null ],
    [ "UpdateName", "class_my_guitar_shop_1_1_repository_1_1_customer_repository.html#a0347184d0702be1614f27d47675365e1", null ]
];