var class_my_guitar_shop_1_1_logic_1_1_guitar_logic =
[
    [ "GuitarLogic", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#a29f89a300e375e4bcce17fe0ae226e67", null ],
    [ "GuitarLogic", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#a54b3df4f0c07036dc594928639a7ed76", null ],
    [ "BrandAveragePrice", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#ae6a4d5aabe4099f3b4c4114b2425a3c3", null ],
    [ "CreateGuitar", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#a132fc55923cdc6afab5e26554dbce64e", null ],
    [ "DeleteGuitar", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#ad26dc83c6b94761e6f5d73f2464d73a1", null ],
    [ "GetAll", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#af53642be6c5398171e50d99c1f71e242", null ],
    [ "GetOne", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#af0b3266375094185bdbcfeaea3708759", null ],
    [ "UpdatePrice", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html#a6aa6fcd0789e7289773ada7cf55939d2", null ]
];