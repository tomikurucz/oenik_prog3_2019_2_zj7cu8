var class_my_guitar_shop_1_1_repository_1_1_purchase_repository =
[
    [ "PurchaseRepository", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html#a471912302fa6f63ab69a7a3619178200", null ],
    [ "CreatePurchase", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html#a7595c017bfb07071fd06dc0483dfbf6f", null ],
    [ "DeletePurchase", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html#a5e1cdc6dc3cd5c368b2d471821de254a", null ],
    [ "GetAll", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html#a1e21669bfe88bcb9f7d42caa84439b8a", null ],
    [ "GetOne", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html#a12f776f95196e524c02a07c37cf4afc2", null ],
    [ "LastId", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html#ad0b937690ef32ba8960603604d669119", null ],
    [ "UpdateName", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html#a62b7135546c74c57928840ef4ab32377", null ]
];