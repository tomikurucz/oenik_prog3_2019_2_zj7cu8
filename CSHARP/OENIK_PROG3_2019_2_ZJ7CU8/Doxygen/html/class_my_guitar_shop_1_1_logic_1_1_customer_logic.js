var class_my_guitar_shop_1_1_logic_1_1_customer_logic =
[
    [ "CustomerLogic", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html#a5bd59c654874aacc41136d545e30bd18", null ],
    [ "CustomerLogic", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html#a97e67854664e652abc040289f07d131a", null ],
    [ "CreateCustomer", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html#a50b03ab073498a952cff9895d423b34e", null ],
    [ "DeleteCustomer", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html#a2e0e74d14e7626113a0287549542d153", null ],
    [ "GetAll", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html#a2a490b677049316d6d36c44031de923f", null ],
    [ "GetOne", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html#a843f63c5b1da8aa2dd0246b9ab17685a", null ],
    [ "UpdateName", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html#a341e75c9beade5bfc679c7a3dc502dd2", null ]
];