var class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests =
[
    [ "CreatePurchase_Test1", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#ad0e264d850c5db326f1d32574a19341e", null ],
    [ "CreatePurchase_Test2", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#a815078b2fba328fbefc68e9b03b29d05", null ],
    [ "CreatePurchase_Test3", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#a3e51974c726dbb7e5a58690b4d5ed64b", null ],
    [ "DeletePurchase_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#a422a0cfee3f248287ccc5c4c1744e494", null ],
    [ "GetAll_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#aaa840c85438e46dfc46734c47ef0dec7", null ],
    [ "GetOne_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#a989d0eb988016f2556a74a8e482035c3", null ],
    [ "PurchasedGuitarData_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#a6a2358197244d7fe68d51ab12919cc88", null ],
    [ "Top3HighestValuePurchase_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#a164e9ead8293156a7c4d2f82007da34e", null ],
    [ "UpdateName_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html#aa4ed3d3c2fa8e1d10e8012b48716d210", null ]
];