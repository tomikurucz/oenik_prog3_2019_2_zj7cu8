var namespace_my_guitar_shop_1_1_logic =
[
    [ "Tests", "namespace_my_guitar_shop_1_1_logic_1_1_tests.html", "namespace_my_guitar_shop_1_1_logic_1_1_tests" ],
    [ "AverageResult", "class_my_guitar_shop_1_1_logic_1_1_average_result.html", "class_my_guitar_shop_1_1_logic_1_1_average_result" ],
    [ "CustomerLogic", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html", "class_my_guitar_shop_1_1_logic_1_1_customer_logic" ],
    [ "FactoryLogic", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html", "class_my_guitar_shop_1_1_logic_1_1_factory_logic" ],
    [ "GuitarLogic", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic" ],
    [ "HighestValuePurchase", "class_my_guitar_shop_1_1_logic_1_1_highest_value_purchase.html", "class_my_guitar_shop_1_1_logic_1_1_highest_value_purchase" ],
    [ "ICustomerLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic.html", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic" ],
    [ "IFactoryLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic.html", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic" ],
    [ "IGuitarLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic" ],
    [ "IPurchaseLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic" ],
    [ "PurchasedGuitarData", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data" ],
    [ "PurchaseLogic", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic" ]
];