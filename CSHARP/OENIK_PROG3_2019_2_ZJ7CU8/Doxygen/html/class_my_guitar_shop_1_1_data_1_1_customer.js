var class_my_guitar_shop_1_1_data_1_1_customer =
[
    [ "Customer", "class_my_guitar_shop_1_1_data_1_1_customer.html#a87ff9e13e0367c80787e6f7d3b03b337", null ],
    [ "ToString", "class_my_guitar_shop_1_1_data_1_1_customer.html#a55170ff2604c2eb65f3a6ed096cdc247", null ],
    [ "Address", "class_my_guitar_shop_1_1_data_1_1_customer.html#a86516e7b90e3c0d7940dba490800aa57", null ],
    [ "Date_of_birth", "class_my_guitar_shop_1_1_data_1_1_customer.html#af39fddb87153099cb5a1937571d8847d", null ],
    [ "Email", "class_my_guitar_shop_1_1_data_1_1_customer.html#ad4959bb68e67b3b001e8970e57b70536", null ],
    [ "Mother", "class_my_guitar_shop_1_1_data_1_1_customer.html#a6b1b2ee53e71c9cc4b22f1ef37690177", null ],
    [ "Name", "class_my_guitar_shop_1_1_data_1_1_customer.html#af7e00ddde690ff4a5c1b8106a4cdd60f", null ],
    [ "Phone_number", "class_my_guitar_shop_1_1_data_1_1_customer.html#ac51e37d3dd0de7276677eddd54bd12fb", null ],
    [ "Place_of_birth", "class_my_guitar_shop_1_1_data_1_1_customer.html#a1485c63d55d26f05bb5678698758e172", null ],
    [ "Purchase", "class_my_guitar_shop_1_1_data_1_1_customer.html#a931eabdc545be3426a1d17c7e6da4feb", null ]
];