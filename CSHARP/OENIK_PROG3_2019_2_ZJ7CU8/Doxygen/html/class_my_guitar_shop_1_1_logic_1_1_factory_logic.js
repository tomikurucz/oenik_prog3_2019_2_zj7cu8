var class_my_guitar_shop_1_1_logic_1_1_factory_logic =
[
    [ "FactoryLogic", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html#a233803695df0af9f474d9456f421e09c", null ],
    [ "FactoryLogic", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html#a0ca6c4f6d9303267ac029dba3349058e", null ],
    [ "CreateFactory", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html#a57053e6580e6d5d1d064214d36365e81", null ],
    [ "DeleteFactory", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html#a1ab2086b0b6bd35ea67b32b2bd1b5312", null ],
    [ "GetAll", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html#a29b8ca00ff5bce6f3ca89bb46e263c04", null ],
    [ "GetOne", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html#a5533927adc145709753958a260de06b5", null ],
    [ "UpdateFounderName", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html#a2ccf9d304fa32dffbf34723059d42247", null ]
];