var class_my_guitar_shop_1_1_data_1_1_guitar =
[
    [ "Guitar", "class_my_guitar_shop_1_1_data_1_1_guitar.html#a592080f7be403434bcf985a85e801548", null ],
    [ "ToString", "class_my_guitar_shop_1_1_data_1_1_guitar.html#af8a6d747faf5be462d7690f68c26ec09", null ],
    [ "Brand", "class_my_guitar_shop_1_1_data_1_1_guitar.html#a1c5a21af804ea9fffd4e41efa0250d0e", null ],
    [ "Factory", "class_my_guitar_shop_1_1_data_1_1_guitar.html#af39b9ffe65932b19260502cdac70ce8e", null ],
    [ "Factory_ID", "class_my_guitar_shop_1_1_data_1_1_guitar.html#ac93ee9621dfa65a0c15ef31cfb34ec6b", null ],
    [ "Guitar_ID", "class_my_guitar_shop_1_1_data_1_1_guitar.html#a06b32988065af18f2857928ebf3e23c9", null ],
    [ "Price", "class_my_guitar_shop_1_1_data_1_1_guitar.html#ab83eb400dc769b52c3c73eaad622089f", null ],
    [ "Producion_time", "class_my_guitar_shop_1_1_data_1_1_guitar.html#a958bc3be017bb9386c2c1dec1ec700ce", null ],
    [ "Purchase", "class_my_guitar_shop_1_1_data_1_1_guitar.html#a6acc52346c2d1936c6f0e3b7e92c31e3", null ],
    [ "Style", "class_my_guitar_shop_1_1_data_1_1_guitar.html#a016789b03fc9276945c8ec2d0f8c466b", null ],
    [ "Type", "class_my_guitar_shop_1_1_data_1_1_guitar.html#ab1e934181d9b8636b56d1a02ad702a63", null ]
];