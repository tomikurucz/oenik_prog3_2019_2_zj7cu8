var class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests =
[
    [ "CreateCustomer_Test1", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html#a51469540bcde60779f9a10e3648261b6", null ],
    [ "CreateCustomer_Test2", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html#a91c672ea614bd316de295fbaf4796571", null ],
    [ "CreateCustomer_Test3", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html#ab459d8b49838d890c32d71a0a5eb187a", null ],
    [ "DeleteCustomer_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html#a8b20b099024159ec8f92ed2f516250fc", null ],
    [ "GetAll_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html#af304c5fdd44e1e57d372122bda96e2d7", null ],
    [ "GetOne_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html#a461074b2281f223a51c5da400f30070e", null ],
    [ "UpdateName_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html#a6c0a134cad9cc6955a8db87459b67748", null ]
];