var interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic =
[
    [ "CreatePurchase", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html#a91973af4b92ae46a293b9102e48ffa37", null ],
    [ "DeletePurchase", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html#ae2de1804939e3916839130e7e2036db0", null ],
    [ "GetAll", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html#a3bf5554471045fed14c399a31e9793e3", null ],
    [ "GetOne", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html#a842212ae5103ecec3aa39212c3a60776", null ],
    [ "PurchasedGuitarData", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html#ac67f08532e1503cb1a958e83e11695dd", null ],
    [ "Top3HighestValuePurchase", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html#a18983a2cc6d28a1b4de57bb4c3bacaaf", null ],
    [ "UpdateName", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html#a8e3a656c5038204b752ba008d2a76697", null ]
];