var dir_09055039b9456fa7249ae884dd74cc16 =
[
    [ "obj", "dir_5cb18a15897c0fba7afd7e1c292a8063.html", "dir_5cb18a15897c0fba7afd7e1c292a8063" ],
    [ "Properties", "dir_b15d1dd2e9e5a22f64df4490dada918c.html", "dir_b15d1dd2e9e5a22f64df4490dada918c" ],
    [ "AverageResult.cs", "_average_result_8cs_source.html", null ],
    [ "CustomerLogic.cs", "_customer_logic_8cs_source.html", null ],
    [ "FactoryLogic.cs", "_factory_logic_8cs_source.html", null ],
    [ "GuitarLogic.cs", "_guitar_logic_8cs_source.html", null ],
    [ "HighestValuePurchase.cs", "_highest_value_purchase_8cs_source.html", null ],
    [ "ICustomerLogic.cs", "_i_customer_logic_8cs_source.html", null ],
    [ "IFactoryLogic.cs", "_i_factory_logic_8cs_source.html", null ],
    [ "IGuitarLogic.cs", "_i_guitar_logic_8cs_source.html", null ],
    [ "IPurchaseLogic.cs", "_i_purchase_logic_8cs_source.html", null ],
    [ "PurchasedGuitarData.cs", "_purchased_guitar_data_8cs_source.html", null ],
    [ "PurchaseLogic.cs", "_purchase_logic_8cs_source.html", null ]
];