var class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests =
[
    [ "BrandAveragePrice_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#a717057e4588aa38c1943ebbc41d4870b", null ],
    [ "CreateGuitar_Test1", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#adf5ac99de95a6902975414feb60df046", null ],
    [ "CreateGuitar_Test2", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#ad9f3a3bec50d1bb6672483ce95b13e30", null ],
    [ "CreateGuitar_Test3", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#a192b835c9f3b196ea779a821719e5fb1", null ],
    [ "DeleteGuitar_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#ad6c4ac7f0be9cb22645986fbd23129a5", null ],
    [ "GetAll_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#a9858034bb4b0e179cb9053e86fa439ad", null ],
    [ "GetOne_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#a2bf0cc13cf25722e7d2f649899e2ae1b", null ],
    [ "UpdatePrice_Test", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html#ae330238def2050015305ed3599c9641b", null ]
];