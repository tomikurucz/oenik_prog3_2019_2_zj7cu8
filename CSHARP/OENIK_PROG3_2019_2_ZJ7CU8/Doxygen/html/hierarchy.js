var hierarchy =
[
    [ "MyGuitarShop.Logic.AverageResult", "class_my_guitar_shop_1_1_logic_1_1_average_result.html", null ],
    [ "MyGuitarShop.Data.Customer", "class_my_guitar_shop_1_1_data_1_1_customer.html", null ],
    [ "MyGuitarShop.Logic.Tests.CustomerLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_customer_logic_tests.html", null ],
    [ "DbContext", null, [
      [ "MyGuitarShop.Data::DatabaseEntities", "class_my_guitar_shop_1_1_data_1_1_database_entities.html", null ]
    ] ],
    [ "MyGuitarShop.Data.Factory", "class_my_guitar_shop_1_1_data_1_1_factory.html", null ],
    [ "MyGuitarShop.Logic.Tests.FactoryLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_factory_logic_tests.html", null ],
    [ "MyGuitarShop.Data.Guitar", "class_my_guitar_shop_1_1_data_1_1_guitar.html", null ],
    [ "MyGuitarShop.Logic.Tests.GuitarLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_guitar_logic_tests.html", null ],
    [ "MyGuitarShop.Logic.HighestValuePurchase", "class_my_guitar_shop_1_1_logic_1_1_highest_value_purchase.html", null ],
    [ "MyGuitarShop.Logic.ICustomerLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_customer_logic.html", [
      [ "MyGuitarShop.Logic.CustomerLogic", "class_my_guitar_shop_1_1_logic_1_1_customer_logic.html", null ]
    ] ],
    [ "MyGuitarShop.Repository.ICustomerRepository", "interface_my_guitar_shop_1_1_repository_1_1_i_customer_repository.html", [
      [ "MyGuitarShop.Repository.CustomerRepository", "class_my_guitar_shop_1_1_repository_1_1_customer_repository.html", null ]
    ] ],
    [ "MyGuitarShop.Logic.IFactoryLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_factory_logic.html", [
      [ "MyGuitarShop.Logic.FactoryLogic", "class_my_guitar_shop_1_1_logic_1_1_factory_logic.html", null ]
    ] ],
    [ "MyGuitarShop.Logic.IGuitarLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_guitar_logic.html", [
      [ "MyGuitarShop.Logic.GuitarLogic", "class_my_guitar_shop_1_1_logic_1_1_guitar_logic.html", null ]
    ] ],
    [ "MyGuitarShop.Logic.IPurchaseLogic", "interface_my_guitar_shop_1_1_logic_1_1_i_purchase_logic.html", [
      [ "MyGuitarShop.Logic.PurchaseLogic", "class_my_guitar_shop_1_1_logic_1_1_purchase_logic.html", null ]
    ] ],
    [ "MyGuitarShop.Repository.IRepository< T >", "interface_my_guitar_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "MyGuitarShop.Repository.IRepository< Factory >", "interface_my_guitar_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyGuitarShop.Repository.IFactoryRepository", "interface_my_guitar_shop_1_1_repository_1_1_i_factory_repository.html", [
        [ "MyGuitarShop.Repository.FactoryRepository", "class_my_guitar_shop_1_1_repository_1_1_factory_repository.html", null ]
      ] ]
    ] ],
    [ "MyGuitarShop.Repository.IRepository< Guitar >", "interface_my_guitar_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyGuitarShop.Repository.IGuitarRepository", "interface_my_guitar_shop_1_1_repository_1_1_i_guitar_repository.html", [
        [ "MyGuitarShop.Repository.GuitarRepository", "class_my_guitar_shop_1_1_repository_1_1_guitar_repository.html", null ]
      ] ]
    ] ],
    [ "MyGuitarShop.Repository.IRepository< Purchase >", "interface_my_guitar_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyGuitarShop.Repository.IPurchaseRepository", "interface_my_guitar_shop_1_1_repository_1_1_i_purchase_repository.html", [
        [ "MyGuitarShop.Repository.PurchaseRepository", "class_my_guitar_shop_1_1_repository_1_1_purchase_repository.html", null ]
      ] ]
    ] ],
    [ "MyGuitarShop.Program", "class_my_guitar_shop_1_1_program.html", null ],
    [ "MyGuitarShop.Data.Purchase", "class_my_guitar_shop_1_1_data_1_1_purchase.html", null ],
    [ "MyGuitarShop.Logic.PurchasedGuitarData", "class_my_guitar_shop_1_1_logic_1_1_purchased_guitar_data.html", null ],
    [ "MyGuitarShop.Logic.Tests.PurchaseLogicTests", "class_my_guitar_shop_1_1_logic_1_1_tests_1_1_purchase_logic_tests.html", null ],
    [ "MyGuitarShop.JavaWeb.Web", "class_my_guitar_shop_1_1_java_web_1_1_web.html", null ]
];