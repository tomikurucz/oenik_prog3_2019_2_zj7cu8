﻿create table Factory
(Factory_ID numeric(20) not null,
Name varchar(50),
Address varchar(50),
Phone_number numeric(9),
Founder_name varchar(50),
Foundation_time date,
constraint Factory_pk primary key(Factory_ID));

create table Guitar
(Guitar_ID numeric(20) not null,
Brand varchar(50),
Type varchar(50),
Style varchar(50),
Price numeric(20),
Producion_time date,
Factory_ID numeric(20) not null,
constraint Guitar_pk primary key(Guitar_ID),
constraint Guitar_fk foreign key(Factory_ID)
references Factory(Factory_ID));

create table Customer
(Name varchar(50),
Date_of_birth date,
Address varchar(50),
Phone_number numeric(9),
Email varchar(50),
Mother varchar(50),
Place_of_birth varchar(50),
constraint Customer_pk primary key(Email)); 

create table Purchase
(Purchase_ID numeric(20) not null,
Guitar_ID numeric(20) not null,
Name varchar(50) not null,
Email varchar(50),
Date_of_birth date not null,
Date_of_purchase date,
constraint Purchase_pk primary key(Purchase_ID),
constraint Purchase_fk1 foreign key(Guitar_ID)
references Guitar(Guitar_ID),
constraint Purchase_fk2 foreign key(Email)
references Customer(Email));

insert into Factory
values(0001,'Gibson','USA Nashville',306297446,'Orville Gibson','1902.10.11');

insert into Factory
values(0002,'Fender','USA Scottsdale 8860 E. Chaparral Road, Suite 100',207665349,'Leo Fender','1946');

insert into Factory
values(0003,'Ibanez','Japán Nagoja',709125816,'Hoshino Gakki','1957');

insert into Factory
values(0004,'Yamaha','Japan Shizuoka Hamamatsu',307846662,'Jamaha Torakuszu','1887.10.12');

insert into Factory
values(0005,'Cort','South Korea Seoul',705556916,'Jung-gyu Park','1973');

insert into Guitar
values(0101,'Gibson','Les Paul Classic Heritage Cherry Sunburst','elektromos',534900,'2019.01.20',0001);

insert into Guitar
values(0102,'Gibson','G-45 Standard Antique Natural','akusztikus',421900,'2019.02.20',0001);

insert into Guitar
values(0103,'Gibson','J-45 Standard 2019 Vintage Sunburst','elektroakusztikus',999900,'2019.03.20',0001);

insert into Guitar
values(0104,'Fender','American Performer Stratocaster RW Honey','elektromos',327900,'2019.04.20',0002);

insert into Guitar
values(0105,'Fender','CD-60S Dreadnought WN Natural','akusztikus',55900,'2019.05.20',0002);

insert into Guitar
values(0106,'Fender','Newporter Player WN Ice Blue Satin','elektroakusztikus',94900,'2019.06.20',0002);

insert into Guitar
values(0107,'Ibanez','JEM77P-BFP','elektromos',474900,'2019.07.20',0003);

insert into Guitar
values(0108,'Ibanez','PF18-WDB','akusztikus',56900,'2019.08.20',0003);

insert into Guitar
values(0109,'Ibanez','AW150CE-OPN','elektroakusztikus',117900,'2019.09.20',0003);

insert into Guitar
values(0110,'Yamaha','Pacifia 112 V YNS','elektromos',78900,'2019.10.20',0004);

insert into Guitar
values(0111,'Yamaha','FG830 TBS','akusztikus',119900,'2019.11.20',0004);

insert into Guitar
values(0112,'Yamaha','FS-TA Brown Sunburst','elektroakusztikus',182900,'2019.12.20',0004);

insert into Guitar
values(0113,'Cort','KX100 IO','elektromos',66900,'2018.01.20',0005);

insert into Guitar
values(0114,'Cort','AD810 SSB','akusztikus',41900,'2018.02.20',0005);

insert into Guitar
values(0115,'Cort','MR710F-NAT','elektroakusztikus',97900,'2018.03.20',0005);

insert into Customer
values('James Marshall Hendrix','1942.11.27','USA Washington Seattle',305223879,'jimihendrix42@gmail.com','Lucille Jeter','USA Washington Seattle');

insert into Customer
values('Eric Patrick Clapton','1945.03.30','Anglia Ripley',702226913,'ericclapton45@gmail.com','Patricia Molly Clapton','Anglia Surrey Ripley');

insert into Customer
values('James Patrick York Page','1944.01.09','Anglia Middlesex Heston',204557702,'jimmypage44@gmail.com','Patricia Page','Anglia Middlesex Heston');

insert into Customer
values('Keith Richards','1943.12.18','Anglia Kent Dartford',203453478,'keithrichards43@gmail.com','Doris Dupree Richards','Anglia Kent Dartford');

insert into Customer
values('Geoffrey Arnold Beck','1944.06.24','Anglia London Wallington',309993336,'jeffbeck44@gmail.com','Ethel Beck','Anglia London Wallington');

insert into Purchase
values(1001,0101,'James Marshall Hendrix','jimihendrix42@gmail.com','1942.11.27','2019.10.01');

insert into Purchase
values(1002,0105,'Eric Patrick Clapton','ericclapton45@gmail.com','1945.03.30','2019.10.02');

insert into Purchase
values(1003,0109,'James Patrick York Page','jimmypage44@gmail.com','1944.01.09','2019.10.03');

insert into Purchase
values(1004,0110,'Keith Richards','keithrichards43@gmail.com','1943.12.18','2019.10.04');

insert into Purchase
values(1005,0114,'Geoffrey Arnold Beck','jeffbeck44@gmail.com','1944.06.24','2019.10.05');

