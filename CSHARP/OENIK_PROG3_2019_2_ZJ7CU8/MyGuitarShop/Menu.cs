﻿using MyGuitarShop.Data;
using MyGuitarShop.Logic;
using MyGuitarShop.JavaWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop
{
    /// <summary>
    /// This class contains the method used to create the program menu
    /// </summary>
    static class Menu
    {
        /// <summary>
        /// this method creates the program menu
        /// </summary>
        public static void CreateMenu()
        {
            GuitarLogic guitarLogic = new GuitarLogic();
            FactoryLogic factoryLogic = new FactoryLogic();
            CustomerLogic customerLogic = new CustomerLogic();
            PurchaseLogic purchaseLogic = new PurchaseLogic();
            Web javaWeb = new Web();

            int fokepernyo;
            do
            {
                Console.WriteLine("#########################################################");
                Console.WriteLine("                          Menü:                          ");
                Console.WriteLine("#########################################################");
                Console.WriteLine("@    1. Listázás                                        @");
                Console.WriteLine("@    2. Hozzáadás                                       @");
                Console.WriteLine("@    3. Törlés                                          @");
                Console.WriteLine("@    4. Módosítás                                       @");
                Console.WriteLine("@    5. Márkánként a gitárok átlagos ára                @");
                Console.WriteLine("@    6. A három legdrágább vásárlás                     @");
                Console.WriteLine("@    7. Megvásárolt gitárok adatai                      @");
                Console.WriteLine("@    8. Webes lekérés                                   @");
                Console.WriteLine("@                                                       @");
                Console.WriteLine("@    9. Kilépés                                         @");
                Console.WriteLine("#########################################################");
                Console.WriteLine();
                Console.Write("Válasszon a funkciók közül:");
                fokepernyo = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (fokepernyo)
                {
                    case 1: //Listázás ág
                        int listazasAlmenu;
                        do
                        {

                            Console.WriteLine("##########################################");
                            Console.WriteLine("Válassza ki, amelyiket listázni szeretné: ");
                            Console.WriteLine("##########################################");
                            Console.WriteLine("@              1. Gitár                  @");
                            Console.WriteLine("@              2. Gyár                   @");
                            Console.WriteLine("@              3. Vásárló                @");
                            Console.WriteLine("@              4. Értékesítés            @");
                            Console.WriteLine("@                                        @");
                            Console.WriteLine("@              5. Vissza                 @");
                            Console.WriteLine("##########################################");
                            Console.WriteLine();
                            Console.Write("Válasszon a funkciók közül:");
                            listazasAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();

                            switch (listazasAlmenu)
                            {
                                case 1: //Gitárok listázása
                                    int bemenet3;
                                    do
                                    {
                                        foreach (var item in guitarLogic.GetAll())
                                        {
                                            Console.WriteLine(item);
                                        }
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet3 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet3 != 5);
                                    break;

                                case 2: //Gyárak listázása
                                    int bemenet4;
                                    do
                                    {
                                        foreach (var item in factoryLogic.GetAll())
                                        {
                                            Console.WriteLine(item);
                                        }
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet4 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet4 != 5);
                                    break;

                                case 3: //Vásárlók listázása
                                    int bemenet5;
                                    do
                                    {
                                        foreach (var item in customerLogic.GetAll())
                                        {
                                            Console.WriteLine(item);
                                        }
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet5 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet5 != 5);
                                    break;

                                case 4: //Értékesítések listázása
                                    int bemenet6;
                                    do
                                    {
                                        foreach (var item in purchaseLogic.GetAll())
                                        {
                                            Console.WriteLine(item);
                                        }
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet6 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet6 != 5);
                                    break;

                            }
                        } while (listazasAlmenu != 5);
                        break;

                    case 2: //Hozzáadás ág
                        int hozzaadasAlmenu;
                        do
                        {
                            Console.WriteLine("###########################################");
                            Console.WriteLine("Válassza ki, amelyikhez hozzáadni szeretne:");
                            Console.WriteLine("###########################################");
                            Console.WriteLine("@               1. Gitár                  @");
                            Console.WriteLine("@               2. Gyár                   @");
                            Console.WriteLine("@               3. Vásárló                @");
                            Console.WriteLine("@               4. Értékesítés            @");
                            Console.WriteLine("@                                         @");
                            Console.WriteLine("@               5. Vissza                 @");
                            Console.WriteLine("###########################################");
                            Console.WriteLine();
                            Console.Write("Válasszon a funkciók közül:");
                            hozzaadasAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();

                            switch (hozzaadasAlmenu)
                            {
                                case 1: //Gitár hozzáadása
                                    int bemenet3;
                                    do
                                    {
                                        Console.WriteLine("Adja meg a gitár adatait!");
                                        Console.Write("\t>Márka: ");
                                        string brand = Console.ReadLine();
                                        Console.Write("\t>Típus: ");
                                        string type = Console.ReadLine();
                                        Console.Write("\t>Fajta: ");
                                        string style = Console.ReadLine();
                                        Console.Write("\t>Ár: ");
                                        int price = int.Parse(Console.ReadLine());
                                        Console.Write("\t>Gyártási idő: ");
                                        DateTime production_time = Convert.ToDateTime(Console.ReadLine());
                                        int factory_id = 0;
                                        if (brand == "Gibson")
                                        {
                                            factory_id = 1;
                                        }
                                        else if (brand == "Fender")
                                        {
                                            factory_id = 2;
                                        }
                                        else if (brand == "Ibanez")
                                        {
                                            factory_id = 3;
                                        }
                                        else if (brand == "Yamaha")
                                        {
                                            factory_id = 4;
                                        }
                                        else if (brand == "Cort")
                                        {
                                            factory_id = 5;
                                        }
                                        guitarLogic.CreateGuitar(new Guitar() { Brand = brand, Type = type, Style = style, Price = price, Producion_time = production_time, Factory_ID = factory_id });
                                        Console.WriteLine();
                                        Console.WriteLine("Új elem hozzáadva!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet3 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet3 != 5);
                                    break;

                                case 2: //Gyár hozzáadása
                                    int bemenet4;
                                    do
                                    {
                                        Console.WriteLine("Adja meg a gyár adatait!");
                                        Console.Write("\t>Név: ");
                                        string name = Console.ReadLine();
                                        Console.Write("\t>Cím: ");
                                        string address = Console.ReadLine();
                                        Console.Write("\t>Telefonszám: ");
                                        int phone_number = int.Parse(Console.ReadLine());
                                        Console.Write("\t>Alapító neve: ");
                                        string founder_name = Console.ReadLine();
                                        Console.Write("\t>Alapítás ideje: ");
                                        DateTime foundation_time = Convert.ToDateTime(Console.ReadLine());
                                        factoryLogic.CreateFactory(new Factory() { Name = name, Address = address, Phone_number = phone_number, Founder_name= founder_name, Foundation_time = foundation_time });

                                        Console.WriteLine("Új elem hozzáadva!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet4 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet4 != 5);
                                    break;

                                case 3: //Vásárló hozzáadása
                                    int bemenet5;
                                    do
                                    {
                                        Console.WriteLine("Adja meg a vásárló adatait!");
                                        Console.Write("\t>Név: ");
                                        string name = Console.ReadLine();
                                        Console.Write("\t>Születési dátum: ");
                                        DateTime date_of_birth = Convert.ToDateTime(Console.ReadLine());
                                        Console.Write("\t>Cím: ");
                                        string address = Console.ReadLine();
                                        Console.Write("\t>Telefonszám: ");
                                        int phone_number = int.Parse(Console.ReadLine());
                                        Console.Write("\t>Email: ");
                                        string email = Console.ReadLine();
                                        Console.Write("\t>Anyja neve: ");
                                        string mother = Console.ReadLine();
                                        Console.Write("\t>Születési hely: ");
                                        string szuletesi_hely = Console.ReadLine();
                                        customerLogic.CreateCustomer(new Customer() { Name = name, Date_of_birth= date_of_birth, Address = address, Phone_number= phone_number, Email = email, Mother= mother, Place_of_birth = szuletesi_hely });

                                        Console.WriteLine("Új elem hozzáadva!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet5 = int.Parse(Console.ReadLine());
                                        Console.Clear();

                                    } while (bemenet5 != 5);
                                    break;

                                case 4: //Értékesítés hozzáadása
                                    int bemenet6;
                                    do
                                    {
                                        Console.WriteLine("Adja meg a vásárlás adatait!");
                                        Console.Write("\t>Gitár id: ");
                                        int guitar_ID = int.Parse(Console.ReadLine());
                                        Console.Write("\t>Név: ");
                                        string name = Console.ReadLine();
                                        Console.Write("\t>Születési dátum: ");
                                        DateTime date_of_birth = Convert.ToDateTime(Console.ReadLine());
                                        Console.Write("\t>Vásárlás időpontja: ");
                                        DateTime date_of_purchase = Convert.ToDateTime(Console.ReadLine());
                                        Console.Write("\t>Email: ");
                                        string email = Console.ReadLine();
                                        purchaseLogic.CreatePurchase(new Purchase() {Guitar_ID = guitar_ID, Name= name, Date_of_birth= date_of_birth, Date_of_purchase= date_of_purchase, Email=email });

                                        Console.WriteLine("Új elem hozzáadva!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet6 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet6 != 5);
                                    break;

                            }
                        } while (hozzaadasAlmenu != 5);
                        break;

                    case 3: //Törlés ág
                        int torlesAlmenu;
                        do
                        {

                            Console.WriteLine("###########################################");
                            Console.WriteLine("Válassza ki, amelyikből törölni szeretne:  ");
                            Console.WriteLine("###########################################");
                            Console.WriteLine("@               1. Gitár                  @");
                            Console.WriteLine("@               2. Gyár                   @");
                            Console.WriteLine("@               3. Vásárló                @");
                            Console.WriteLine("@               4. Értékesítés            @");
                            Console.WriteLine("@                                         @");
                            Console.WriteLine("@               5. Vissza                 @");
                            Console.WriteLine("###########################################");
                            Console.WriteLine();
                            Console.Write("Válasszon a funkciók közül:");
                            torlesAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();

                            switch (torlesAlmenu)
                            {
                                case 1: //Gitár törlése
                                    int bemenet3;
                                    do
                                    {
                                        Console.Write("ID: ");
                                        int id = int.Parse(Console.ReadLine());
                                        guitarLogic.DeleteGuitar(id);
                                        Console.WriteLine("Törölve!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet3 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet3 != 5);
                                    break;

                                case 2: //Gyár törlése
                                    int bemenet4;
                                    do
                                    {
                                        Console.Write("ID: ");
                                        int id = int.Parse(Console.ReadLine());
                                        factoryLogic.DeleteFactory(id);
                                        Console.WriteLine("Törölve!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet4 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet4 != 5);
                                    break;

                                case 3: //Vásárló törlése
                                    int bemenet5;
                                    do
                                    {
                                        Console.Write("Email: ");
                                        string email = Console.ReadLine();
                                        customerLogic.DeleteCustomer(email);
                                        Console.WriteLine("Törölve!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet5 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet5 != 5);
                                    break;


                                case 4: //Értékesítés törlése
                                    int bemenet6;
                                    do
                                    {
                                        Console.Write("ID: ");
                                        int id = int.Parse(Console.ReadLine());
                                        purchaseLogic.DeletePurchase(id);
                                        Console.WriteLine("Törölve!");
                                        Console.WriteLine();
                                        Console.WriteLine("@    5. Vissza       @");
                                        Console.WriteLine();
                                        Console.Write("Kilépéshez írja be, hogy 5: ");
                                        bemenet6 = int.Parse(Console.ReadLine());
                                        Console.Clear();
                                    } while (bemenet6 != 5);
                                    break;

                            }
                        } while (torlesAlmenu != 5);
                        break;

                    case 4: //Módosítás ág
                        int modositasAlmenu;
                        do
                        {

                            Console.WriteLine("###########################################");
                            Console.WriteLine("Válassza ki, amelyiket módosítani szeretné:");
                            Console.WriteLine("###########################################");
                            Console.WriteLine("@               1. Gitár                  @");
                            Console.WriteLine("@               2. Gyár                   @");
                            Console.WriteLine("@               3. Vásárló                @");
                            Console.WriteLine("@               4. Értékesítés            @");
                            Console.WriteLine("@                                         @");
                            Console.WriteLine("@               5. Vissza                 @");
                            Console.WriteLine("###########################################");
                            Console.WriteLine();
                            Console.Write("Válasszon a funkciók közül:");
                            modositasAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();
                            int ID;
                            switch (modositasAlmenu)
                            {
                                case 1: //Gitár adat módosítás
                                    Console.Clear();
                                    Console.Write("ID: ");
                                    ID = int.Parse(Console.ReadLine());
                                    Console.Write("Új ár: ");
                                    int ujar = int.Parse(Console.ReadLine());
                                    guitarLogic.UpdatePrice(ID, ujar);
                                    Console.WriteLine("Módosítás elmentve!");
                                    System.Threading.Thread.Sleep(2000);
                                    Console.Clear();
                                    break;


                                case 2: //Gyár adat módosítás

                                    Console.Clear();
                                    Console.Write("ID: ");
                                    ID = int.Parse(Console.ReadLine());
                                    Console.Write("Alapító neve: ");
                                    string alapito_neve = Console.ReadLine();
                                    factoryLogic.UpdateFounderName(ID, alapito_neve);
                                    Console.WriteLine("Módosítás elmentve!");
                                    System.Threading.Thread.Sleep(2000);
                                    Console.Clear();
                                    break;

                                case 3: //Vásárló adat módosítás
                                    Console.Clear();
                                    Console.Write("Email: ");
                                    string email = Console.ReadLine();
                                    Console.Write("Név:");
                                    string name = Console.ReadLine();
                                    customerLogic.UpdateName(email, name);
                                    Console.WriteLine("Módosítás elmentve!");
                                    System.Threading.Thread.Sleep(2000);
                                    Console.Clear();
                                    break;



                                case 4: //Értékesítés adat módosítás
                                    Console.Clear();
                                    Console.Write("ID: ");
                                    ID = int.Parse(Console.ReadLine());
                                    Console.Write("Név: ");
                                    string newname = Console.ReadLine();
                                    purchaseLogic.UpdateName(ID, newname);
                                    Console.WriteLine("Módosítás elmentve!");
                                    System.Threading.Thread.Sleep(2000);
                                    Console.Clear();
                                    
                                    break;

                            }
                        } while (modositasAlmenu != 5);
                        break;

                    case 5: //Márkánként a gitárok átlagos ára
                        int atlagArAlmenu;
                        do
                        {
                            foreach (var item in guitarLogic.BrandAveragePrice())
                            {
                                Console.WriteLine(item);
                            }
                            Console.WriteLine();
                            Console.WriteLine("@    5. Vissza       @");
                            Console.WriteLine();
                            Console.Write("Kilépéshez írja be, hogy 5: ");
                            atlagArAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();
                        } while (atlagArAlmenu != 5);
                        break;

                    case 6: //A három legdrágább vásárlás
                        int legdragabbVasarlasAlmenu;
                        do
                        {
                            foreach (var item in purchaseLogic.Top3HighestValuePurchase())
                            {
                                Console.WriteLine(item);
                            }
                            Console.WriteLine();
                            Console.WriteLine("@    5. Vissza       @");
                            Console.WriteLine();
                            Console.Write("Kilépéshez írja be, hogy 5: ");
                            legdragabbVasarlasAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();
                        } while (legdragabbVasarlasAlmenu != 5);
                        break;

                    case 7: //Megrendelők akik vásároltak már gitárt
                        int megrendelokAdataiAlmenu;
                        do
                        {
                            foreach (var item in purchaseLogic.PurchasedGuitarData())
                            {
                                Console.WriteLine(item);
                            }
                            Console.WriteLine();
                            Console.WriteLine("@    5. Vissza       @");
                            Console.WriteLine();
                            Console.Write("Kilépéshez írja be, hogy 5: ");
                            megrendelokAdataiAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();
                        } while (megrendelokAdataiAlmenu != 5);
                        break;

                    case 8: //Webes lekérés
                        int webesLekeresAlmenu;
                        do
                        {
                            Console.Write("Ha szeretné tudni, hogy a megvásárolni kívánt gitár megrendelhető-e üzletünkben, kérem írja be az azonosítóját: ");
                            int id = int.Parse(Console.ReadLine());
                            List<Web> guitar = Web.AvailableGuitar(id);
                            if (guitar[0].Available)
                            {
                                Console.WriteLine("A gitár megrendelhető az üzletünkben.");
                            }
                            else
                            {
                                Console.WriteLine("A gitár nem rendelhető meg az üzletünkben.");
                            }
                            Console.WriteLine();
                            Console.WriteLine("@    5. Vissza       @");
                            Console.WriteLine();
                            Console.Write("Kilépéshez írja be, hogy 5: ");
                            webesLekeresAlmenu = int.Parse(Console.ReadLine());
                            Console.Clear();
                        } while (webesLekeresAlmenu != 5);
                        break;
                }

            } while (fokepernyo != 9);
        }
    }
}
