﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace MyGuitarShop.JavaWeb
{
    /// <summary>
    /// this class is required for java connection
    /// </summary>
    public class Web
    {
        /// <summary>
        /// stores the information received from the java endpoint
        /// </summary>
        public bool Available { get; set; }

        /// <summary>
        /// returns with information from the java endpoint
        /// </summary>
        /// <param name="guitarID"></param>
        /// <returns></returns>
        public static List<Web> AvailableGuitar(int guitarID)
        {
            XDocument xd = XDocument.Load("http://localhost:8080/OENIK_PROG3_2019_2_ZJ7CU8/AvailableGuitar?id="+guitarID);
            return xd.Descendants("Guitar").Select(x => new Web()
            { Available=Convert.ToBoolean(x.Element("Available")?.Value)}).ToList();
            
        }
    }
}
