﻿using MyGuitarShop.Data;
using MyGuitarShop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// GuitarLogic must implement the methods specified in the interface
    /// </summary>
    public interface IGuitarLogic
    {
        void CreateGuitar(Guitar guitar);
        List<Guitar> GetAll();
        Guitar GetOne(int ID);
        void UpdatePrice(int ID, int newprice);
        void DeleteGuitar(int ID);
        IList<AverageResult> BrandAveragePrice(); 
    }
}
