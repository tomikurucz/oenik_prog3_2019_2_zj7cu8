﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// this class stores the fields of the BrandAveragePrice method
    /// </summary>
    public class AverageResult
    {
        /// <summary>
        /// this property stores the guitar's brand
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// this property stores the guitar's average price
        /// </summary>
        public double AveragePrice { get; set; }

        /// <summary>
        /// the class is written in the form described in the method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "Márka: " + Brand + ", Átlagos ár: " + AveragePrice;
        }
        
    }
}
