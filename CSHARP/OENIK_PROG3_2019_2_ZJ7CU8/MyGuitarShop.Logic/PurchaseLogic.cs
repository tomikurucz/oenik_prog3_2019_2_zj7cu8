﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// this class calls the PurchaseRepository methods
    /// </summary>
    public class PurchaseLogic : IPurchaseLogic
    {
        IPurchaseRepository purchaseRepository;
        IGuitarRepository guitarRepository;

        /// <summary>
        /// creates two DatabaseEntities object that is required for PurchaseRepository and GuitarRepository class methods
        /// </summary>
        public PurchaseLogic()
        {
            purchaseRepository = new PurchaseRepository(new DatabaseEntities());
            guitarRepository = new GuitarRepository(new DatabaseEntities());
        }

        /// <summary>
        /// needed to pass the mocked repository used in testing
        /// </summary>
        /// <param name="purchaseRepository"></param>
        public PurchaseLogic(IPurchaseRepository purchaseRepository)
        {
            this.purchaseRepository = purchaseRepository;
        }

        /// <summary>
        /// needed to pass the mocked repository used in testing
        /// </summary>
        /// <param name="purchaseRepository"></param>
        /// <param name="guitarRepository"></param>
        public PurchaseLogic(IPurchaseRepository purchaseRepository, IGuitarRepository guitarRepository)
        {
            this.purchaseRepository = purchaseRepository;
            this.guitarRepository = guitarRepository;
        }

        /// <summary>
        /// creates a Purchase object
        /// </summary>
        /// <param name="purchase"></param>
        public void CreatePurchase(Purchase purchase)
        {
            if (purchase.Date_of_birth == null || purchase.Date_of_purchase == null || purchase.Email == null || purchase.Name == null)
            {
                throw new ArgumentException("Wrong data");
            }
            purchase.Purchase_ID = purchaseRepository.LastId() + 1;
            purchaseRepository.CreatePurchase(purchase);
        }

        /// <summary>
        /// deletes a Purchase object
        /// </summary>
        /// <param name="ID"></param>
        public void DeletePurchase(int ID)
        {
            purchaseRepository.DeletePurchase(ID);
        }

        /// <summary>
        /// returns all Purchase in a list
        /// </summary>
        /// <returns></returns>
        public List<Purchase> GetAll()
        {
            return purchaseRepository.GetAll().ToList();
        }

        /// <summary>
        /// returns the qualifying Purchase object
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Purchase GetOne(int ID)
        {
            return purchaseRepository.GetOne(ID);
        }

        /// <summary>
        /// modifies the name field of the corresponding Purchase object
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="newname"></param>
        public void UpdateName(int ID, string newname)
        {
            purchaseRepository.UpdateName(ID, newname);
        }

        /// <summary>
        /// returns a list of top 3 highest value purchase
        /// </summary>
        /// <returns></returns>
        public IList<HighestValuePurchase> Top3HighestValuePurchase()
        {
            var Purchases = purchaseRepository.GetAll().ToList();
            var Guitars = guitarRepository.GetAll().ToList();

            var q = from p in Purchases
                    join g in Guitars on p.Guitar_ID equals g.Guitar_ID
                    orderby g.Price descending
                    select new HighestValuePurchase()
                    {
                        Guitar_ID = (int)g.Guitar_ID,
                        Price = (int)g.Price
                    };

            return q.Take(3).ToList();
        }

        /// <summary>
        /// returns a list of purchased guitar data
        /// </summary>
        /// <returns></returns>
        public IList<PurchasedGuitarData> PurchasedGuitarData()
        {
            var Purchases = purchaseRepository.GetAll().ToList();
            var Guitars = guitarRepository.GetAll().ToList();

            var q = from p in Purchases
                    join g in Guitars on p.Guitar_ID equals g.Guitar_ID
                    select new PurchasedGuitarData()
                    {
                        Guitar_ID = (int)g.Guitar_ID,
                        Name=p.Name,
                        Date_of_purchase=(System.DateTime)p.Date_of_purchase,
                        Brand = g.Brand,
                        Type = g.Type,
                        Style = g.Style,
                        Price = (int)g.Price,
                        Producion_time = (System.DateTime)g.Producion_time,
                        Factory_ID = (int)g.Factory_ID
                    };
            return q.ToList();

        }
    }
}
