﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// this class stores the fields of the PurchasedGuitarData method
    /// </summary>
    public class PurchasedGuitarData
    {
        /// <summary>
        /// this property stores the guitar's ID
        /// </summary>
        public int Guitar_ID { get; set; }

        /// <summary>
        /// this property stores the guitar's brand
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// this property stores the guitar's type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// this property stores the guitar's style
        /// </summary>
        public string Style { get; set; }

        /// <summary>
        /// this property stores the guitar's price
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// this property stores the guitar's production time
        /// </summary>
        public DateTime Producion_time { get; set; }

        /// <summary>
        /// this property stores the guitar's factory ID
        /// </summary>
        public int Factory_ID { get; set; }

        /// <summary>
        /// this property stores the date of purchase
        /// </summary>
        public DateTime Date_of_purchase { get; set; }

        /// <summary>
        /// this property stores the customer's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the class is written in the form described in the method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "Gitár id: " + Guitar_ID + "\n\tVásárló neve: " + Name + "\n\tVásárlás időpontja: " + Date_of_purchase + "\n\tMárka: " + Brand + "\n\tTípus: " + Type + "\n\tFajta: " + Style + "\n\tÁr: " + Price + "\n\tGyártási idő: " + Producion_time + "\n\tGyár id: " + Factory_ID + "\n";
        }
    }
}
