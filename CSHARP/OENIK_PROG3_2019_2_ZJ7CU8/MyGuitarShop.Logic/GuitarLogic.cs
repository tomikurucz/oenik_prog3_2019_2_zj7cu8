﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// this class calls the GuitarRepository methods
    /// </summary>
    public class GuitarLogic : IGuitarLogic
    {
        IGuitarRepository guitarRepository;

        /// <summary>
        /// creates a DatabaseEntities object that is required for GuitarRepository class methods
        /// </summary>
        public GuitarLogic()
        {
            guitarRepository = new GuitarRepository(new DatabaseEntities());
        }

        /// <summary>
        /// needed to pass the mocked repository used in testing
        /// </summary>
        /// <param name="guitarRepository"></param>
        public GuitarLogic(IGuitarRepository guitarRepository)
        {
            this.guitarRepository = guitarRepository;
        }

        /// <summary>
        /// returns a list of average guitar prices per brand
        /// </summary>
        /// <returns></returns>
        public IList<AverageResult> BrandAveragePrice()
        {
            var q = from guitar in GetAll()
                    group guitar by guitar.Brand into g
                    select new AverageResult()
                    {
                        Brand = g.Key,
                        AveragePrice = Math.Round((double)g.Average(guitar => guitar.Price))
                    };
            return q.ToList();
        }

        /// <summary>
        /// creates a Guitar object
        /// </summary>
        /// <param name="guitar"></param>
        public void CreateGuitar(Guitar guitar)
        {
            if (guitar.Brand== null||guitar.Price==null||guitar.Producion_time==null||guitar.Style==null||guitar.Type==null)
            {
                throw new ArgumentException("Wrong data");
            }
            guitar.Guitar_ID = guitarRepository.LastID() + 1;
            guitarRepository.CreateGuitar(guitar);
        }

        /// <summary>
        /// deletes a Guitar object
        /// </summary>
        /// <param name="ID"></param>
        public void DeleteGuitar(int ID)
        {
            guitarRepository.DeleteGuitar(ID);
        }

        /// <summary>
        /// returns all Guitars in a list
        /// </summary>
        /// <returns></returns>
        public List<Guitar> GetAll()
        {
            return guitarRepository.GetAll().ToList();
        }

        /// <summary>
        /// returns the qualifying Guitar object
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Guitar GetOne(int ID)
        {
            return guitarRepository.GetOne(ID);
        }

        /// <summary>
        /// modifies the price field of the corresponding Guitar object
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="newprice"></param>
        public void UpdatePrice(int ID, int newprice)
        {
            guitarRepository.UpdatePrice(ID, newprice);
        }
    }
}
