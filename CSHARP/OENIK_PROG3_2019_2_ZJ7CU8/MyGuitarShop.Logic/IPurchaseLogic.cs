﻿using MyGuitarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// PurchaseLogic must implement the methods specified in the interface
    /// </summary>
    public interface IPurchaseLogic
    {
        void CreatePurchase(Purchase purchase);
        List<Purchase> GetAll();
        Purchase GetOne(int ID);
        void UpdateName(int ID, string newname);
        void DeletePurchase(int ID);
        IList<HighestValuePurchase> Top3HighestValuePurchase();
        IList<PurchasedGuitarData> PurchasedGuitarData();
    }
}
