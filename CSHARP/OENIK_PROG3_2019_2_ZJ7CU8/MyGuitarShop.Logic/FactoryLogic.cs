﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// this class calls the FactoryRepository methods
    /// </summary>
    public class FactoryLogic : IFactoryLogic
    {
        IFactoryRepository factoryRepository;

        /// <summary>
        /// creates a DatabaseEntities object that is required for FactoryRepository class methods
        /// </summary>
        public FactoryLogic()
        {
            factoryRepository = new FactoryRepository(new DatabaseEntities());
        }

        /// <summary>
        /// needed to pass the mocked repository used in testing
        /// </summary>
        /// <param name="factoryRepository"></param>
        public FactoryLogic(IFactoryRepository factoryRepository)
        {
            this.factoryRepository = factoryRepository;
        }

        /// <summary>
        /// creates a Factory object
        /// </summary>
        /// <param name="factory"></param>
        public void CreateFactory(Factory factory)
        {
            if (factory.Address==null||factory.Foundation_time==null||factory.Founder_name==null||factory.Name==null||factory.Phone_number==null)
            {
                throw new ArgumentException("Wrong data");
            }
            factory.Factory_ID = factoryRepository.LastID() + 1;
            factoryRepository.CreateFactory(factory);
        }

        /// <summary>
        /// deletes a Factory object
        /// </summary>
        /// <param name="ID"></param>
        public void DeleteFactory(int ID)
        {
            factoryRepository.DeleteFactory(ID);
        }

        /// <summary>
        /// returns all Factory in a list
        /// </summary>
        /// <returns></returns>
        public List<Factory> GetAll()
        {
            return factoryRepository.GetAll().ToList();
        }

        /// <summary>
        /// returns the qualifying Factory object
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Factory GetOne(int ID)
        {
            return factoryRepository.GetOne(ID);
        }

        /// <summary>
        /// modifies the FounderName field of the corresponding Factory object
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="newname"></param>
        public void UpdateFounderName(int ID, string newname)
        {
            factoryRepository.UpdateFounderName(ID, newname);
        }
    }
}
