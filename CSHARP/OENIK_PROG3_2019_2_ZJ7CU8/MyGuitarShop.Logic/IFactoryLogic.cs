﻿using MyGuitarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// FactoryLogic must implement the methods specified in the interface
    /// </summary>
    public interface IFactoryLogic
    {
        void CreateFactory(Factory factory);
        List<Factory> GetAll();
        Factory GetOne(int ID);
        void UpdateFounderName(int ID, string newname);
        void DeleteFactory(int ID);
    }
}
