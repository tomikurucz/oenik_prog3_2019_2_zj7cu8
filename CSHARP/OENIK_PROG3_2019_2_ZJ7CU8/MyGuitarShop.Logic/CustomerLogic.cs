﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// this class calls the CustomerRepository methods
    /// </summary>
    public class CustomerLogic : ICustomerLogic
    {
        ICustomerRepository customerRepository;

        /// <summary>
        /// creates a DatabaseEntities object that is required for CustomerRepository class methods
        /// </summary>
        public CustomerLogic()
        {
            customerRepository = new CustomerRepository(new DatabaseEntities());
        }

        /// <summary>
        /// needed to pass the mocked repository used in testing
        /// </summary>
        /// <param name="customerRepository"></param>
        public CustomerLogic(ICustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        /// <summary>
        /// creates a Customer object
        /// </summary>
        /// <param name="customer"></param>
        public void CreateCustomer(Customer customer)
        {
            if (customer.Address == null || customer.Date_of_birth == null || customer.Email == null||customer.Mother==null||customer.Name==null||customer.Phone_number==null||customer.Place_of_birth==null)
            {
                throw new ArgumentException("Wrong data");
            }
            customerRepository.CreateCustomer(customer);
        }

        /// <summary>
        /// deletes a Customer object
        /// </summary>
        /// <param name="email"></param>
        public void DeleteCustomer(string email)
        {
            customerRepository.DeleteCustomer(email);
        }

        /// <summary>
        /// returns all Customers in a list
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetAll()
        {
            return customerRepository.GetAll().ToList();
        }

        /// <summary>
        /// returns the qualifying Customer object
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Customer GetOne(string email)
        {
            return customerRepository.GetOne(email);
        }

        /// <summary>
        /// modifies the name field of the corresponding Customer object
        /// </summary>
        /// <param name="email"></param>
        /// <param name="newname"></param>
        public void UpdateName(string email, string newname)
        {
            customerRepository.UpdateName(email, newname);
        }

        
    }
}
