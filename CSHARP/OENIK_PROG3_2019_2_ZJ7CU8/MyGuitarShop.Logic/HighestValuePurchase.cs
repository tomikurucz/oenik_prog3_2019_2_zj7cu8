﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// this class stores the fields of the Top3HighestValuePurchase method
    /// </summary>
    public class HighestValuePurchase
    {
        /// <summary>
        /// this property stores the guitar's ID
        /// </summary>
        public int Guitar_ID { get; set; }

        /// <summary>
        /// this property stores the guitar's price
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// the class is written in the form described in the method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "Gitár id: " + Guitar_ID + ", Ár: " + Price;
        }
    }
}
