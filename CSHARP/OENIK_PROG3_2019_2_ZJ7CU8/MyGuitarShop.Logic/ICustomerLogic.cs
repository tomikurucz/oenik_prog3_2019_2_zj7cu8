﻿using MyGuitarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic
{
    /// <summary>
    /// CustomerLogic must implement the methods specified in the interface
    /// </summary>
    public interface ICustomerLogic
    {
        void CreateCustomer(Customer customer);
        List<Customer> GetAll();
        Customer GetOne(string email);
        void UpdateName(string email, string newname);
        void DeleteCustomer(string email);
    }
}
