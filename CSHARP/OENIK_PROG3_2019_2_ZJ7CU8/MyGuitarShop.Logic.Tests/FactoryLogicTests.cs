﻿using Moq;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic.Tests
{
    /// <summary>
    /// this class contains tests for class FactoryLogic
    /// </summary>
    [TestFixture]
    class FactoryLogicTests
    {
        /// <summary>
        /// this method tests that an instance without parameter throw an ArgumentException
        /// </summary>
        [Test]
        public void CreateFactory_Test1()
        {
            Mock<IFactoryRepository> mock = new Mock<IFactoryRepository>();
            FactoryLogic logic = new FactoryLogic(mock.Object);
            Assert.That(() => logic.CreateFactory(new Factory()), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// this method tests that an instance with all parameters does not throw an ArgumentException
        /// </summary>
        [Test]
        public void CreateFactory_Test2()
        {
            Mock<IFactoryRepository> mock = new Mock<IFactoryRepository>();
            FactoryLogic logic = new FactoryLogic(mock.Object);
            Assert.DoesNotThrow(() => logic.CreateFactory(new Factory()
            {
                Address = "address",
                Factory_ID = 1,
                Foundation_time = DateTime.Now,
                Founder_name = "name",
                Name = "name",
                Phone_number = 1

            }));
        }

        /// <summary>
        /// this method tests that if we create a Factory object then the CreateFactory method is called only once
        /// </summary>
        [Test]
        public void CreateFactory_Test3()
        {
            Mock<IFactoryRepository> mock = new Mock<IFactoryRepository>();
            FactoryLogic logic = new FactoryLogic(mock.Object);
            logic.CreateFactory(new Factory()
            {
                Address = "address",
                Factory_ID = 1,
                Foundation_time = DateTime.Now,
                Founder_name = "name",
                Name = "name",
                Phone_number = 1

            });
            mock.Verify(m => m.CreateFactory(It.IsAny<Factory>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if all Factory objects are requested then the GetAll method is called only once
        /// </summary>
        [Test]
        public void GetAll_Test()
        {
            Mock<IFactoryRepository> mock = new Mock<IFactoryRepository>();
            FactoryLogic logic = new FactoryLogic(mock.Object);
            logic.GetAll();
            mock.Verify(m => m.GetAll(), Times.Once);
        }

        /// <summary>
        /// this method tests that if a Factory object is requested then the GetOne method is called only once
        /// </summary>
        [Test]
        public void GetOne_Test()
        {
            Mock<IFactoryRepository> mock = new Mock<IFactoryRepository>();
            FactoryLogic logic = new FactoryLogic(mock.Object);
            logic.GetOne(It.IsAny<int>());
            mock.Verify(m => m.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we update Customer object's founder name then the UpdateFounderName method is called only once
        /// </summary>
        [Test]
        public void UpdateFounderName_Test()
        {
            Mock<IFactoryRepository> mock = new Mock<IFactoryRepository>();
            FactoryLogic logic = new FactoryLogic(mock.Object);
            logic.UpdateFounderName(It.IsAny<int>(), It.IsAny<string>());
            mock.Verify(m => m.UpdateFounderName(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we delete a Factory object then the DeleteFactory method is called only once
        /// </summary>
        [Test]
        public void DeleteFactory_Test()
        {
            Mock<IFactoryRepository> mock = new Mock<IFactoryRepository>();
            FactoryLogic logic = new FactoryLogic(mock.Object);
            logic.DeleteFactory(It.IsAny<int>());
            mock.Verify(m => m.DeleteFactory(It.IsAny<int>()), Times.Once);
        }
    }
}
