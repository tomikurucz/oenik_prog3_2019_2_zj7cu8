﻿using Moq;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic.Tests
{
    /// <summary>
    /// this class contains tests for class GuitarLogic
    /// </summary>
    [TestFixture]
    public class GuitarLogicTests
    {
        /// <summary>
        /// this method tests that an instance without parameter throw an ArgumentException
        /// </summary>
        [Test]
        public void CreateGuitar_Test1()
        {
            Mock<IGuitarRepository> mock = new Mock<IGuitarRepository>();
            GuitarLogic logic = new GuitarLogic(mock.Object);
            Assert.That(() => logic.CreateGuitar(new Guitar()), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// this method tests that an instance with all parameters does not throw an ArgumentException
        /// </summary>
        [Test]
        public void CreateGuitar_Test2()
        {
            Mock<IGuitarRepository> mock = new Mock<IGuitarRepository>();
            GuitarLogic logic = new GuitarLogic(mock.Object);
            Assert.DoesNotThrow(() => logic.CreateGuitar(new Guitar()
            {
                Brand = "brand",
                Factory_ID = 1,
                Price = 1000,
                Producion_time = DateTime.Now,
                Style = "style",
                Type = "type"
            }));
        }

        /// <summary>
        /// this method tests that if we create a Guitar object then the CreateGuitar method is called only once
        /// </summary>
        [Test]
        public void CreateGuitar_Test3()
        {
            Mock<IGuitarRepository> mock = new Mock<IGuitarRepository>();
            GuitarLogic logic = new GuitarLogic(mock.Object);

            logic.CreateGuitar(new Guitar()
            {
                Brand = "brand",
                Factory_ID = 1,
                Price = 1000,
                Producion_time = DateTime.Now,
                Style = "style",
                Type = "type"
            });

            mock.Verify(m => m.CreateGuitar(It.IsAny<Guitar>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if all Guitar objects are requested then the GetAll method is called only once
        /// </summary>
        [Test]
        public void GetAll_Test()
        {
            Mock<IGuitarRepository> mock = new Mock<IGuitarRepository>();
            GuitarLogic logic = new GuitarLogic(mock.Object);
            logic.GetAll();
            mock.Verify(m => m.GetAll(), Times.Once);
        }

        /// <summary>
        /// this method tests that if a Guitar object is requested then the GetOne method is called only once
        /// </summary>
        [Test]
        public void GetOne_Test()
        {
            Mock<IGuitarRepository> mock = new Mock<IGuitarRepository>();
            GuitarLogic logic = new GuitarLogic(mock.Object);
            logic.GetOne(It.IsAny<int>());
            mock.Verify(m => m.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we update Customer object's price then the UpdatePrice method is called only once
        /// </summary>
        [Test]
        public void UpdatePrice_Test()
        {
            Mock<IGuitarRepository> guitarRepository = new Mock<IGuitarRepository>();
            GuitarLogic logic = new GuitarLogic(guitarRepository.Object);

            logic.UpdatePrice(It.IsAny<int>(), It.IsAny<int>());

            guitarRepository.Verify(r => r.UpdatePrice(It.IsAny<int>(), It.IsAny<int>()), Times.Once);

        }

        /// <summary>
        /// this method tests that if we delete a Guitar object then the DeleteGuitar method is called only once
        /// </summary>
        [Test]
        public void DeleteGuitar_Test()
        {
            Mock<IGuitarRepository> mock = new Mock<IGuitarRepository>();
            GuitarLogic logic = new GuitarLogic(mock.Object);
            logic.DeleteGuitar(It.IsAny<int>());
            mock.Verify(m => m.DeleteGuitar(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we ask for the average price of a guitar per brand then the GetAll method is called only once and checks if result of BrandAveragePrice method is correct
        /// </summary>
        [Test]
        public void BrandAveragePrice_Test()
        {
            Mock<IGuitarRepository> mock = new Mock<IGuitarRepository>();
            GuitarLogic guitarLogic = new GuitarLogic(mock.Object);
            List<Guitar> testdata = new List<Guitar>();
            testdata.Add(new Guitar()
            {
                Brand = "brand",
                Factory_ID = 1,
                Price = 1000,
                Producion_time = DateTime.Now,
                Style = "style",
                Type = "type"
            });
            testdata.Add(new Guitar()
            {
                Brand = "brand",
                Factory_ID = 1,
                Price = 3000,
                Producion_time = DateTime.Now,
                Style = "style",
                Type = "type"
            });
            mock.Setup(m => m.GetAll()).Returns(testdata.AsQueryable);

            var result = guitarLogic.BrandAveragePrice()[0].ToString();

            mock.Verify(m => m.GetAll(), Times.Once);
            Assert.NotNull(result);
            Assert.AreEqual("Márka: brand, Átlagos ár: 2000",result);

        }
    }
}
