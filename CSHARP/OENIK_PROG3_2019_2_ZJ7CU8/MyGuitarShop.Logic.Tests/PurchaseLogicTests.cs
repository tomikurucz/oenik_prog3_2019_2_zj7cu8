﻿using Moq;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic.Tests
{
    /// <summary>
    /// this class contains tests for class PurchaseLogic
    /// </summary>
    [TestFixture]
    class PurchaseLogicTests
    {
        /// <summary>
        /// this method tests that an instance without parameter throw an ArgumentException
        /// </summary>
        [Test]
        public void CreatePurchase_Test1()
        {
            Mock<IPurchaseRepository> mock = new Mock<IPurchaseRepository>();
            PurchaseLogic logic = new PurchaseLogic(mock.Object);
            Assert.That(() => logic.CreatePurchase(new Purchase()), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// this method tests that an instance with all parameters does not throw an ArgumentException
        /// </summary>
        [Test]
        public void CreatePurchase_Test2()
        {
            Mock<IPurchaseRepository> mock = new Mock<IPurchaseRepository>();
            PurchaseLogic logic = new PurchaseLogic(mock.Object);
            Assert.DoesNotThrow(() => logic.CreatePurchase(new Purchase()
            {
                Date_of_birth=DateTime.Now,
                Date_of_purchase=DateTime.Now,
                Email="email",
                Guitar_ID=1,
                Name="name",
                Purchase_ID=1
            }));
        }

        /// <summary>
        /// this method tests that if we create a Purchase object then the CreatePurchase method is called only once
        /// </summary>
        [Test]
        public void CreatePurchase_Test3()
        {
            Mock<IPurchaseRepository> mock = new Mock<IPurchaseRepository>();
            PurchaseLogic logic = new PurchaseLogic(mock.Object);
            logic.CreatePurchase(new Purchase()
            {
                Date_of_birth = DateTime.Now,
                Date_of_purchase = DateTime.Now,
                Email = "email",
                Guitar_ID = 1,
                Name = "name",
                Purchase_ID = 1
            });
            mock.Verify(m => m.CreatePurchase(It.IsAny<Purchase>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if all Purchase objects are requested then the GetAll method is called only once
        /// </summary>
        [Test]
        public void GetAll_Test()
        {
            Mock<IPurchaseRepository> mock = new Mock<IPurchaseRepository>();
            PurchaseLogic logic = new PurchaseLogic(mock.Object);
            logic.GetAll();
            mock.Verify(m => m.GetAll(), Times.Once);
        }

        /// <summary>
        /// this method tests that if a Purchase object is requested then the GetOne method is called only once
        /// </summary>
        [Test]
        public void GetOne_Test()
        {
            Mock<IPurchaseRepository> mock = new Mock<IPurchaseRepository>();
            PurchaseLogic logic = new PurchaseLogic(mock.Object);
            logic.GetOne(It.IsAny<int>());
            mock.Verify(m => m.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we update Purchase object's name then the UpdateName method is called only once
        /// </summary>
        [Test]
        public void UpdateName_Test()
        {
            Mock<IPurchaseRepository> mock = new Mock<IPurchaseRepository>();
            PurchaseLogic logic = new PurchaseLogic(mock.Object);
            logic.UpdateName(It.IsAny<int>(), It.IsAny<string>());
            mock.Verify(m => m.UpdateName(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we delete a Customer object then the DeleteCustomer method is called only once
        /// </summary>
        [Test]
        public void DeletePurchase_Test()
        {
            Mock<IPurchaseRepository> mock = new Mock<IPurchaseRepository>();
            PurchaseLogic logic = new PurchaseLogic(mock.Object);
            logic.DeletePurchase(It.IsAny<int>());
            mock.Verify(m => m.DeletePurchase(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we ask for the highest value purchase then the GetAll method is called only once and checks if result of Top3HighestValuePurchase method is correct
        /// </summary>
        [Test]
        public void Top3HighestValuePurchase_Test()
        {
            Mock<IPurchaseRepository> purchaseRepositoryMock = new Mock<IPurchaseRepository>();
            Mock<IGuitarRepository> guitarRepositoryMock = new Mock<IGuitarRepository>();
            PurchaseLogic purchaseLogic = new PurchaseLogic(purchaseRepositoryMock.Object, guitarRepositoryMock.Object);
            List<Guitar> guitarTestdataset = new List<Guitar>();
            guitarTestdataset.Add(new Guitar()
            {
                Guitar_ID=1,
                Brand = "brand",
                Factory_ID = 1,
                Price = 1000,
                Producion_time = DateTime.Now,
                Style = "style",
                Type = "type"
            });
            guitarTestdataset.Add(new Guitar()
            {
                Guitar_ID=2,
                Brand = "brand",
                Factory_ID = 1,
                Price = 3000,
                Producion_time = DateTime.Now,
                Style = "style",
                Type = "type"
            });
            List<Purchase> purchaseTestdataset = new List<Purchase>();
            purchaseTestdataset.Add(new Purchase()
            {
                Purchase_ID = 1,
                Guitar_ID = 1,
                Name = "name",
                Email = "email",
                Date_of_birth = DateTime.Now,
                Date_of_purchase = DateTime.Now
            });
            purchaseTestdataset.Add(new Purchase()
            {
                Purchase_ID = 1,
                Guitar_ID = 2,
                Name = "name",
                Email = "email",
                Date_of_birth = DateTime.Now,
                Date_of_purchase = DateTime.Now
            });
            purchaseRepositoryMock.Setup(p => p.GetAll()).Returns(purchaseTestdataset.AsQueryable);
            guitarRepositoryMock.Setup(g => g.GetAll()).Returns(guitarTestdataset.AsQueryable);

            var result = purchaseLogic.Top3HighestValuePurchase();

            purchaseRepositoryMock.Verify(p => p.GetAll(), Times.Once);
            guitarRepositoryMock.Verify(g => g.GetAll(), Times.Once);
            Assert.NotNull(result);
            Assert.That(result[0].Guitar_ID == 2);
            Assert.That(result[1].Guitar_ID == 1);
            Assert.That(result[0].Price == 3000);
            Assert.That(result[1].Price == 1000);
            Assert.AreEqual("Gitár id: 2, Ár: 3000", result[0].ToString());
            Assert.AreEqual("Gitár id: 1, Ár: 1000", result[1].ToString());

        }

        /// <summary>
        /// this method tests that if we ask for purchased guitar's data then the GetAll method is called only once and checks if result of PurchasedGuitarData method is correct
        /// </summary>
        [Test]
        public void PurchasedGuitarData_Test()
        {
            Mock<IPurchaseRepository> purchaseRepositoryMock = new Mock<IPurchaseRepository>();
            Mock<IGuitarRepository> guitarRepositoryMock = new Mock<IGuitarRepository>();
            PurchaseLogic purchaseLogic = new PurchaseLogic(purchaseRepositoryMock.Object, guitarRepositoryMock.Object);
            List<Guitar> guitarTestdataset = new List<Guitar>();
            guitarTestdataset.Add(new Guitar()
            {
                Guitar_ID = 1,
                Brand = "brand",
                Factory_ID = 1,
                Price = 1000,
                Producion_time = new DateTime(2019, 11, 29, 0, 00, 00),
                Style = "style",
                Type = "type"
            });
            List<Purchase> purchaseTestdataset = new List<Purchase>();
            purchaseTestdataset.Add(new Purchase()
            {
                Purchase_ID = 1,
                Guitar_ID = 1,
                Name = "name",
                Email = "email",
                Date_of_birth = new DateTime(2019,11,29,0,00,00),
                Date_of_purchase = new DateTime(2019, 11, 29, 0, 00, 00)
            });
            purchaseRepositoryMock.Setup(p => p.GetAll()).Returns(purchaseTestdataset.AsQueryable);
            guitarRepositoryMock.Setup(g => g.GetAll()).Returns(guitarTestdataset.AsQueryable);

            var result = purchaseLogic.PurchasedGuitarData();

            purchaseRepositoryMock.Verify(p => p.GetAll(), Times.Once);
            guitarRepositoryMock.Verify(g => g.GetAll(), Times.Once);
            Assert.NotNull(result);
            Assert.That(result[0].Guitar_ID == 1);
            Assert.That(result[0].Price == 1000);
            Assert.AreEqual("Gitár id: 1\n\tVásárló neve: name\n\tVásárlás időpontja: 2019. 11. 29. 0:00:00\n\tMárka: brand\n\tTípus: type\n\tFajta: style\n\tÁr: 1000\n\tGyártási idő: 2019. 11. 29. 0:00:00\n\tGyár id: 1\n", result[0].ToString());
        }
    }
}
