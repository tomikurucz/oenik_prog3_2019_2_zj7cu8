﻿using Moq;
using MyGuitarShop.Data;
using MyGuitarShop.Repository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Logic.Tests
{
    /// <summary>
    /// this class contains tests for class CustomerLogic
    /// </summary>
    [TestFixture]
    class CustomerLogicTests
    {
        /// <summary>
        /// this method tests that an instance without parameter throw an ArgumentException
        /// </summary>
        [Test]
        public void CreateCustomer_Test1()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            CustomerLogic logic = new CustomerLogic(mock.Object);
            Assert.That(() => logic.CreateCustomer(new Customer()), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// this method tests that an instance with all parameters does not throw an ArgumentException
        /// </summary>
        [Test]
        public void CreateCustomer_Test2()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            CustomerLogic logic = new CustomerLogic(mock.Object);
            Assert.DoesNotThrow(() => logic.CreateCustomer(new Customer()
            {
                Address = "address",
                Date_of_birth = DateTime.Now,
                Email="email",
                Mother="mother",
                Place_of_birth="place",
                Name = "name",
                Phone_number = 1
            })) ;
        }

        /// <summary>
        /// this method tests that if we create a Customer object then the CreateCustomer method is called only once
        /// </summary>
        [Test]
        public void CreateCustomer_Test3()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            CustomerLogic logic = new CustomerLogic(mock.Object);
            logic.CreateCustomer(new Customer()
            {
                Address = "address",
                Date_of_birth = DateTime.Now,
                Email = "email",
                Mother = "mother",
                Place_of_birth = "place",
                Name = "name",
                Phone_number = 1
            });
            mock.Verify(m => m.CreateCustomer(It.IsAny<Customer>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if all Customer objects are requested then the GetAll method is called only once
        /// </summary>
        [Test]
        public void GetAll_Test()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            CustomerLogic logic = new CustomerLogic(mock.Object);
            logic.GetAll();
            mock.Verify(m => m.GetAll(), Times.Once);
        }

        /// <summary>
        /// this method tests that if a Customer object is requested then the GetOne method is called only once
        /// </summary>
        [Test]
        public void GetOne_Test()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            CustomerLogic logic = new CustomerLogic(mock.Object);
            logic.GetOne(It.IsAny<string>());
            mock.Verify(m => m.GetOne(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we update Customer object's name then the UpdateName method is called only once
        /// </summary>
        [Test]
        public void UpdateName_Test()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            CustomerLogic logic = new CustomerLogic(mock.Object);
            logic.UpdateName(It.IsAny<string>(), It.IsAny<string>());
            mock.Verify(m => m.UpdateName(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// this method tests that if we delete a Customer object then the DeleteCustomer method is called only once
        /// </summary>
        [Test]
        public void DeleteCustomer_Test()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            CustomerLogic logic = new CustomerLogic(mock.Object);
            logic.DeleteCustomer(It.IsAny<string>());
            mock.Verify(m => m.DeleteCustomer(It.IsAny<string>()), Times.Once);
        }
    }
}
