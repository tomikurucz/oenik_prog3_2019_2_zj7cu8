﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// class handling Purchase table in the database
    /// </summary>
    public class PurchaseRepository : IPurchaseRepository
    {
        DatabaseEntities db;

        /// <summary>
        /// sets db value
        /// </summary>
        /// <param name="DB"></param>
        public PurchaseRepository(DatabaseEntities DB)
        {
            db = DB;
        }

        /// <summary>
        /// adds a new Purchase to the database
        /// </summary>
        /// <param name="purchase"></param>
        public void CreatePurchase(Purchase purchase)
        {
            db.Purchase.Add(purchase);
            db.SaveChanges();
        }

        /// <summary>
        /// deletes a Purchase from the database
        /// </summary>
        /// <param name="ID"></param>
        public void DeletePurchase(int ID)
        {
            db.Purchase.Remove(GetOne(ID));
            db.SaveChanges();
        }

        /// <summary>
        /// returns all Purchases
        /// </summary>
        /// <returns></returns>
        public IQueryable<Purchase> GetAll()
        {
            return db.Purchase;
        }

        /// <summary>
        /// returns the qualifying Purchase object
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Purchase GetOne(int ID)
        {
            return db.Purchase.Where(p => p.Purchase_ID == ID).FirstOrDefault();
        }

        /// <summary>
        /// returns the last Purchase ID
        /// </summary>
        /// <returns></returns>
        public int LastId()
        {
            return (int)db.Purchase.Max(p => p.Purchase_ID);
        }

        /// <summary>
        /// modifies the name field of the corresponding Purchase in the database
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="newname"></param>
        public void UpdateName(int ID, string newname)
        {
            var q = GetOne(ID);
            q.Name = newname;
            db.SaveChanges();
        }
    }
}
