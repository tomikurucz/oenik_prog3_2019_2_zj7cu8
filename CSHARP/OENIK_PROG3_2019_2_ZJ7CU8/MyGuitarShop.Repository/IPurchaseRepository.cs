﻿using MyGuitarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// PurchaseRepository must implement the methods specified in the interface
    /// </summary>
    public interface IPurchaseRepository:IRepository<Purchase>
    {
        void CreatePurchase(Purchase purchase);
        void UpdateName(int ID, string newname);
        void DeletePurchase(int ID);
        int LastId();
    }
}
