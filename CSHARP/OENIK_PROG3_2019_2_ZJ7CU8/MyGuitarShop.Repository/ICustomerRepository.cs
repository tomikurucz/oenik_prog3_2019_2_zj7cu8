﻿using MyGuitarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// CustomerRepository must implement the methods specified in the interface
    /// </summary>
    public interface ICustomerRepository
    {
        void CreateCustomer(Customer customer);
        Customer GetOne(string email);
        IQueryable<Customer> GetAll();
        void UpdateName(string Email,string newname);
        void DeleteCustomer(string email);
    }
}
