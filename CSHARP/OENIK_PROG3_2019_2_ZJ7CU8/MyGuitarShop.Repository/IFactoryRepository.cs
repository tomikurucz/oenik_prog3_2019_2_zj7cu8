﻿using MyGuitarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// FactoryRepository must implement the methods specified in the interface
    /// </summary>
    public interface IFactoryRepository:IRepository<Factory>
    {
        void CreateFactory(Factory factory);
        void UpdateFounderName(int ID, string newname);
        void DeleteFactory(int ID);
        int LastID();
    }
}
