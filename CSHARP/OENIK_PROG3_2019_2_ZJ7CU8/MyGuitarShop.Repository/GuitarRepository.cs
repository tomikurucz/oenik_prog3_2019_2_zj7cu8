﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// class handling Guitar table in the database
    /// </summary>
    public class GuitarRepository : IGuitarRepository
    {
        DatabaseEntities db;

        /// <summary>
        /// sets db value
        /// </summary>
        /// <param name="DB"></param>
        public GuitarRepository(DatabaseEntities DB)
        {
            db = DB;
        }

        /// <summary>
        /// adds a new Guitar to the database
        /// </summary>
        /// <param name="guitar"></param>
        public void CreateGuitar(Guitar guitar)
        {
            db.Guitar.Add(guitar);
            db.SaveChanges();
        }

        /// <summary>
        /// deletes a Guitar from the database
        /// </summary>
        /// <param name="ID"></param>
        public void DeleteGuitar(int ID)
        {
            db.Guitar.Remove(GetOne(ID));
            db.SaveChanges();
        }

        /// <summary>
        /// returns all Guitars
        /// </summary>
        /// <returns></returns>
        public IQueryable<Guitar> GetAll()
        {
            return db.Guitar;
        }

        /// <summary>
        /// returns the qualifying Guitar object
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Guitar GetOne(int ID)
        {
            return db.Guitar.Where(g => g.Guitar_ID == ID).FirstOrDefault();
        }

        /// <summary>
        /// returns the last Guitar ID
        /// </summary>
        /// <returns></returns>
        public int LastID()
        {
            return (int)db.Guitar.Max(g => g.Guitar_ID);
        }

        /// <summary>
        /// modifies the price field of the corresponding Guitar in the database
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="newprice"></param>
        public void UpdatePrice(int ID, int newprice)
        {
            var guitar = GetOne(ID);
            guitar.Price = newprice;
            db.SaveChanges();
        }
    }
}
