﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// class handling Factory table in the database
    /// </summary>
    public class FactoryRepository : IFactoryRepository
    {
        DatabaseEntities db;

        /// <summary>
        /// sets db value
        /// </summary>
        /// <param name="DB"></param>
        public FactoryRepository(DatabaseEntities DB)
        {
            db = DB;
        }

        /// <summary>
        /// adds a new Factory to the database
        /// </summary>
        /// <param name="factory"></param>
        public void CreateFactory(Factory factory)
        {
            db.Factory.Add(factory);
            db.SaveChanges();
        }

        /// <summary>
        /// deletes a Factory from the database
        /// </summary>
        /// <param name="ID"></param>
        public void DeleteFactory(int ID)
        {
            db.Factory.Remove(GetOne(ID));
            db.SaveChanges();
        }

        /// <summary>
        /// returns all Factory
        /// </summary>
        /// <returns></returns>
        public IQueryable<Factory> GetAll()
        {
            return db.Factory;
        }

        /// <summary>
        /// returns the qualifying Factory object
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Factory GetOne(int ID)
        {
            return db.Factory.Where(f => f.Factory_ID == ID).FirstOrDefault();
        }

        /// <summary>
        /// returns the last factory ID
        /// </summary>
        /// <returns></returns>
        public int LastID()
        {
            return (int)db.Factory.Max(f => f.Factory_ID);
        }

        /// <summary>
        /// modifies the FounderName field of the corresponding Factory in the database
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="newname"></param>
        public void UpdateFounderName(int ID, string newname)
        {
            var q = GetOne(ID);
            q.Founder_name = newname;
            db.SaveChanges();
        }
    }
}
