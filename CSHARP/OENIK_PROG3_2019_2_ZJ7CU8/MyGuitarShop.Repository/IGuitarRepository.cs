﻿using MyGuitarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// GuitarRepository must implement the methods specified in the interface
    /// </summary>
    public interface IGuitarRepository:IRepository<Guitar>
    {
        void CreateGuitar(Guitar guitar);
        void UpdatePrice(int ID, int newprice);
        void DeleteGuitar(int ID);
        int LastID();
        
    }
}
