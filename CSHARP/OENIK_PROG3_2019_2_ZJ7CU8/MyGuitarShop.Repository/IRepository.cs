﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// repositories must implement the methods specified in the interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T:class
    {
        T GetOne(int ID);
        IQueryable<T> GetAll();
    }
}
