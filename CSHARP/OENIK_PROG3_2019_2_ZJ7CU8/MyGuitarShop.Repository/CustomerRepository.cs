﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGuitarShop.Data;

namespace MyGuitarShop.Repository
{
    /// <summary>
    /// class handling Customer table in the database
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        DatabaseEntities db;

        /// <summary>
        /// sets db value
        /// </summary>
        /// <param name="DB"></param>
        public CustomerRepository(DatabaseEntities DB)
        {
            db = DB;
        }

        /// <summary>
        /// adds a new Customer to the database
        /// </summary>
        /// <param name="customer"></param>
        public void CreateCustomer(Customer customer)
        {
            db.Customer.Add(customer);
            db.SaveChanges();
        }

        /// <summary>
        /// deletes a Customer from the database
        /// </summary>
        /// <param name="email"></param>
        public void DeleteCustomer(string email)
        {
            db.Customer.Remove(GetOne(email));
            db.SaveChanges();
        }

        /// <summary>
        /// returns all Customers
        /// </summary>
        /// <returns></returns>
        public IQueryable<Customer> GetAll()
        {
            return db.Customer;
        }

        /// <summary>
        /// returns the qualifying Customer object
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Customer GetOne(string email)
        {
            return db.Customer.Where(c => c.Email == email).FirstOrDefault();
        }

        /// <summary>
        /// modifies the name field of the corresponding Customer in the database
        /// </summary>
        /// <param name="email"></param>
        /// <param name="newname"></param>
        public void UpdateName(string email, string newname)
        {
            var q = GetOne(email);
            q.Name = newname;
            db.SaveChanges();
        }
    }
}
