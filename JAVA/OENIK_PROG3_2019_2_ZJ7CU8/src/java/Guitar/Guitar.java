/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Guitar;

import javax.xml.bind.annotation.*;

/**
 *
 * @author Kurucz Tamás
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Guitar {
    
    static final long serialVersionUID = 1L;
    
    @XmlElement
    int Guitar_ID;

    public Guitar(int Guitar_ID) {
        this.Guitar_ID = Guitar_ID;
    }

    public int getGuitar_ID() {
        return Guitar_ID;
    }
    
    
    
}
